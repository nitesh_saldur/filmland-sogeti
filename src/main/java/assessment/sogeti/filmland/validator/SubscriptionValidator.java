package assessment.sogeti.filmland.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import assessment.sogeti.filmland.exception.FilmlandException;
import assessment.sogeti.filmland.jpa.repository.CustomerRepository;
import assessment.sogeti.filmland.jpa.repository.CustomerSubscriptionRepository;
import assessment.sogeti.filmland.jpa.repository.SubscriptionRepository;
import assessment.sogeti.filmland.rest.model.subscription.CreateSubscriptionRequest;
import assessment.sogeti.filmland.rest.model.subscription.TransferSubscriptionRequest;

/**
 * SubscriptionValidator class is responsible for validating creation/transfer of subscription
 */
@Component
public class SubscriptionValidator {

  /** The customer subscription repository. */
  @Autowired
  private CustomerSubscriptionRepository customerSubscriptionRepository;

  /** The subscription repository. */
  @Autowired
  private SubscriptionRepository subscriptionRepository;

  /** The filmland customer repository. */
  @Autowired
  private CustomerRepository filmlandCustomerRepository;

  /**
   * Validate creation of subscription. Check if the customer with email exist. Check if category is
   * already subscribed to the customer. Check if category is amongst the one provided by filmland.
   *
   * @param createSubscriptionRequest the create subscription request
   */
  public void validateCreateSubscription(CreateSubscriptionRequest createSubscriptionRequest) {
    validateEmail(createSubscriptionRequest.getEmail());
    checkIfSubscribed(createSubscriptionRequest.getEmail(),
        createSubscriptionRequest.getAvailableCategory(), false);
    checkIfSubscriptionAvailable(createSubscriptionRequest.getAvailableCategory());
  }

  /**
   * Validate transfer of subscription from one customer to another. Check if both the customers
   * with email exist. Check if the category is valid. Check if first customer is subscribed to
   * customer. Check if second customer doesnt have subscription to category.
   *
   * @param transferSubscriptionRequest the transfer subscription request
   */
  public void validateTransferSubscription(
      TransferSubscriptionRequest transferSubscriptionRequest) {
    validateEmail(transferSubscriptionRequest.getEmail());
    validateEmail(transferSubscriptionRequest.getCustomer());
    checkIfSubscriptionAvailable(transferSubscriptionRequest.getSubscribedCategory());
    checkIfSubscribed(transferSubscriptionRequest.getEmail(),
        transferSubscriptionRequest.getSubscribedCategory(), true);
    checkIfSubscribed(transferSubscriptionRequest.getCustomer(),
        transferSubscriptionRequest.getSubscribedCategory(), false);
  }

  /**
   * Validate email. Check the record of customer with email exist in database.
   *
   * @param email the email
   */
  private void validateEmail(String email) {
    if (!filmlandCustomerRepository.existsById(email)) {
      throw new FilmlandException("Email " + email + " does not exist");
    }
  }

  /**
   * Check if subscribed. Check if customer is subscribed or not to the category.
   *
   * @param email the email
   * @param category the category
   * @param shouldBeSubscribed the should be subscribed
   */
  private void checkIfSubscribed(String email, String category, boolean shouldBeSubscribed) {
    boolean isSubscribed = customerSubscriptionRepository.existsByEmailAndName(email, category);
    if (isSubscribed != shouldBeSubscribed) {
      throw new FilmlandException("Subscription " + category + " is "
          + (shouldBeSubscribed ? "not" : "already") + " subscribed");
    }
  }

  /**
   * Check if subscription available in the filmland repository.
   *
   * @param category the category
   */
  private void checkIfSubscriptionAvailable(String category) {
    boolean isAvalilable = subscriptionRepository.existsAvailableSubscription(category);
    if (!isAvalilable) {
      throw new FilmlandException("Subscription " + category + " is not available");
    }
  }

}
