package assessment.sogeti.filmland.security.config;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import assessment.sogeti.filmland.jpa.entity.Customer;
import assessment.sogeti.filmland.jpa.repository.CustomerRepository;

/**
 * AuthUserDetailsService class is responsible to fetch the user details and wrap it around the set
 * the user details in User required by authenticator
 */
@Service
public class AuthUserDetailsService implements UserDetailsService {

  /** The filmland customer repository. */
  @Autowired
  private CustomerRepository filmlandCustomerRepository;

  /**
   * Load user by username. Searches by email and if not found throws the exception
   *
   * @param username the username
   * @return the user details
   * @throws UsernameNotFoundException the username not found exception
   */
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Customer filmlandCustomer = filmlandCustomerRepository.findById(username).orElseThrow(
        () -> new UsernameNotFoundException(String.format("'%s' doesnt exist", username)));
    return new User(filmlandCustomer.getEmail(), filmlandCustomer.getPassword(), new ArrayList<>());
  }

}
