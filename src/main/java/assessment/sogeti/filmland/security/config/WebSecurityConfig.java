package assessment.sogeti.filmland.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


/**
 * WebSecurityConfig class is responsible to set the auth filter and authentication exception
 * handler for invalid logins or tokens. All the URL need to be authorized except for '/login'
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  /** The filmland auth entry point. */
  @Autowired
  private AuthEntryPoint filmlandAuthEntryPoint;

  /** The filmland user service. */
  @Autowired
  private AuthUserDetailsService filmlandUserService;

  /** The filmland auth request filter. */
  @Autowired
  private AuthRequestFilter filmlandAuthRequestFilter;

  /**
   * Configure global.
   *
   * @param auth the auth
   * @throws Exception the exception
   */
  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    // configure AuthenticationManager so that it knows from where to load
    // user for matching credentials
    // Use BCryptPasswordEncoder
    auth.userDetailsService(filmlandUserService).passwordEncoder(passwordEncoder());
  }

  /**
   * Password encoder.
   *
   * @return the password encoder
   */
  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  /**
   * Authentication manager bean.
   *
   * @return the authentication manager
   * @throws Exception the exception
   */
  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  /**
   * Configure the http security context to disable CORS and CSRF. Sets the Auth Entry Pint for
   * handling Authentication Failure. Sets the Filter for every request.
   *
   * @param httpSecurity the http security
   * @throws Exception the exception
   */
  @Override
  protected void configure(HttpSecurity httpSecurity) throws Exception {
    httpSecurity.cors().and().csrf().disable().exceptionHandling()
        .authenticationEntryPoint(filmlandAuthEntryPoint).and().sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
        .anyRequest().authenticated();
    httpSecurity.addFilterBefore(filmlandAuthRequestFilter,
        UsernamePasswordAuthenticationFilter.class);
  }

  /**
   * Configure to ignore '/login' uri.
   *
   * @param webSecurity the web security
   */
  @Override
  public void configure(WebSecurity webSecurity) {
    webSecurity.ignoring().antMatchers(HttpMethod.POST, "/login");
  }
}
