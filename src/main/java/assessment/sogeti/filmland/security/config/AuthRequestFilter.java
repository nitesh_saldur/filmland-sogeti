package assessment.sogeti.filmland.security.config;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;


/**
 * The Class AuthRequestFilter chains for every request to check if JWT token in present in the
 * header. Validates the token and sets the authentication
 */
@Component
public class AuthRequestFilter extends OncePerRequestFilter {


  /** The filmland user service. */
  @Autowired
  private AuthUserDetailsService filmlandUserService;

  /** The filmland token util. */
  @Autowired
  private AuthTokenUtil filmlandTokenUtil;

  /**
   * Do filter internal.
   *
   * @param request the request
   * @param response the response
   * @param filterChain the filter chain
   * @throws ServletException the servlet exception
   * @throws IOException Signals that an I/O exception has occurred.
   */
  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {
    String jwtToken = extractJWTTokenFromHeader(request);
    String username = retreiveUsernameFromToken(jwtToken);
    // Once we get the token validate it.
    if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
      UserDetails userDetails = this.filmlandUserService.loadUserByUsername(username);
      // if token is valid configure Spring Security to manually set
      // authentication
      if (filmlandTokenUtil.validateToken(jwtToken, userDetails)) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
            new UsernamePasswordAuthenticationToken(userDetails, null,
                userDetails.getAuthorities());
        usernamePasswordAuthenticationToken
            .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        // After setting the Authentication in the context, we specify
        // that the current user is authenticated. So it passes the
        // Spring Security Configurations successfully.
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
      }
    }
    filterChain.doFilter(request, response);
  }

  /**
   * Extract JWT token from header.
   *
   * @param request the request
   * @return the string
   */
  private String extractJWTTokenFromHeader(HttpServletRequest request) {
    final String requestTokenHeader = request.getHeader("Authorization");
    // JWT Token is in the form "Bearer token". Remove Bearer word and get
    // only the Token
    if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
      return requestTokenHeader.substring(7);
    } else {
      logger.warn("JWT Token does not begin with Bearer String");
    }
    return null;
  }

  /**
   * Retreive username from token.
   *
   * @param jwtToken the jwt token
   * @return the string
   */
  private String retreiveUsernameFromToken(String jwtToken) {
    if (jwtToken != null) {
      try {
        return filmlandTokenUtil.getUsernameFromToken(jwtToken);
      } catch (Exception e) {
        System.out.println("Unable to get username");
      }
    }
    return null;
  }
}
