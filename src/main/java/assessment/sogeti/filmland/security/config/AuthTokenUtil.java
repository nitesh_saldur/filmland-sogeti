package assessment.sogeti.filmland.security.config;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * The Class AuthTokenUtil is used to validate and generate the JWT token.
 */
@Component
public class AuthTokenUtil {


  /** The Constant JWT_TOKEN_VALIDITY. */
  // 20 mins validity
  public static final long JWT_TOKEN_VALIDITY = TimeUnit.MINUTES.toMillis(20);

  /** The secret. */
  @Value("${filmland.jwt.secret}")
  private String secret;

  /**
   * Gets the username from token.
   *
   * @param token the token
   * @return the username from token
   */
  // retrieve username from jwt token
  public String getUsernameFromToken(String token) {
    return getClaimFromToken(token, Claims::getSubject);
  }

  /**
   * Gets the expiration date from token.
   *
   * @param token the token
   * @return the expiration date from token
   */
  // retrieve expiration date from jwt token
  public Date getExpirationDateFromToken(String token) {
    return getClaimFromToken(token, Claims::getExpiration);
  }

  /**
   * Gets the claim from token.
   *
   * @param <T> the generic type
   * @param token the token
   * @param claimsResolver the claims resolver
   * @return the claim from token
   */
  public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
    final Claims claims = getAllClaimsFromToken(token);
    return claimsResolver.apply(claims);
  }

  /**
   * Gets the all claims from token.
   *
   * @param token the token
   * @return the all claims from token
   */
  // for retrieveing any information from token we will need the secret key
  private Claims getAllClaimsFromToken(String token) {
    return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
  }

  /**
   * Checks if is token expired.
   *
   * @param token the token
   * @return the boolean
   */
  // check if the token has expired
  private Boolean isTokenExpired(String token) {
    final Date expiration = getExpirationDateFromToken(token);
    return expiration.before(new Date());
  }

  /**
   * Generate token.
   *
   * @param userDetails the user details
   * @return the string
   */
  // generate token for user
  public String generateToken(UserDetails userDetails) {
    Map<String, Object> claims = new HashMap<>();
    return doGenerateToken(claims, userDetails.getUsername());
  }

  /**
   * Do generate token.
   *
   * @param claims the claims
   * @param subject the subject
   * @return the string
   */

  private String doGenerateToken(Map<String, Object> claims, String subject) {
    // Define claims of the token, Issuer, Expiration, Subject, and the Subject
    // Sign the JWT using the HS512 algorithm and secret key.
    // compaction of the JWT to a URL-safe string
    long timeInMillis = System.currentTimeMillis();
    return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(timeInMillis))
        .setExpiration(new Date(timeInMillis + JWT_TOKEN_VALIDITY))
        .signWith(SignatureAlgorithm.HS512, secret).compact();
  }

  /**
   * Validate token. If the token is not expired and subject from claim matches the email then its
   * valid token
   *
   * @param token the token
   * @param userDetails the user details
   * @return the boolean
   */
  public Boolean validateToken(String token, UserDetails userDetails) {
    final String username = getUsernameFromToken(token);
    return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
  }

}
