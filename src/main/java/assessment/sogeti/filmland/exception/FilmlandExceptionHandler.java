package assessment.sogeti.filmland.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * The Class FilmlandExceptionHandler.
 */
@ControllerAdvice
public class FilmlandExceptionHandler {

  /**
   * Exception handler for FilmlandException
   *
   * @param exception the exception
   * @return the response entity with proper message
   */
  @ExceptionHandler(value = FilmlandException.class)
  public ResponseEntity<ErrorMessage> exception(FilmlandException exception) {
    return new ResponseEntity<>(new ErrorMessage(exception.getMessage()), HttpStatus.BAD_REQUEST);
  }

  /**
   * Exception handler for BadCredentialsException
   * 
   * @param BadCredentialsException for unauthorized entry
   * @return the response entity with proper message
   */
  @ExceptionHandler(value = BadCredentialsException.class)
  public ResponseEntity<ErrorMessage> exception(BadCredentialsException exception) {
    return new ResponseEntity<>(new ErrorMessage(exception.getMessage()), HttpStatus.UNAUTHORIZED);
  }
}
