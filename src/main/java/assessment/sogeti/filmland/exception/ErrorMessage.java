package assessment.sogeti.filmland.exception;

/**
 * The Class ErrorMessage.
 */
public class ErrorMessage {

  /** The message. */
  private String message;

  /**
   * Gets the message.
   *
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * Instantiates a new error message.
   *
   * @param message the message
   */
  public ErrorMessage(String message) {
    this.message = message;
  }
}
