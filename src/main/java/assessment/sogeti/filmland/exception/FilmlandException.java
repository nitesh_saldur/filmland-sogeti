package assessment.sogeti.filmland.exception;

/**
 * The Class FilmlandException.
 */
public class FilmlandException extends RuntimeException {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /**
   * Instantiates a new kalah exception.
   *
   * @param message the message
   */
  public FilmlandException(String message) {
    super(message);
  }
}
