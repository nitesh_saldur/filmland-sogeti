package assessment.sogeti.filmland.jpa;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * The Class JPAConfig.
 */
@EnableJpaRepositories(basePackages = {"assessment.sogeti.filmland.jpa.repository"})
@EntityScan(basePackages = {"assessment.sogeti.filmland.jpa.entity"})
@Configuration
public class JPAConfig {

}
