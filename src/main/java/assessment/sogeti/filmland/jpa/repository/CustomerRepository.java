package assessment.sogeti.filmland.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import assessment.sogeti.filmland.jpa.entity.Customer;

/**
 * JPA CustomerRepository to find customer.
 */
public interface CustomerRepository extends JpaRepository<Customer, String> {

}
