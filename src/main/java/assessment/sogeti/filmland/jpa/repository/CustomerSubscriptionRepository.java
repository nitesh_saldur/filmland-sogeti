package assessment.sogeti.filmland.jpa.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import assessment.sogeti.filmland.jpa.entity.CustomerSubscription;
import assessment.sogeti.filmland.jpa.entity.CustomerSubscriptionID;

/**
 * JPA CustomerSubscriptionRepository.
 */
public interface CustomerSubscriptionRepository
    extends JpaRepository<CustomerSubscription, CustomerSubscriptionID> {

  /**
   * Find by email.
   *
   * @param email the email
   * @return the list
   */
  public List<CustomerSubscription> findByEmail(String email);

  /**
   * Find by customer's email and name of the subscription.
   *
   * @param email the email
   * @param name the name
   * @return the customer subscription
   */
  public CustomerSubscription findByEmailAndName(String email, String name);

  /**
   * Exists customer's by email and name of the subscription.
   *
   * @param email the email
   * @param name the name
   * @return true, if successful
   */
  public boolean existsByEmailAndName(String email, String name);
}
