package assessment.sogeti.filmland.jpa.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import assessment.sogeti.filmland.jpa.entity.Subscription;

/**
 * The Interface SubscriptionRepository.
 */
public interface SubscriptionRepository extends JpaRepository<Subscription, String> {

  /**
   * Find available subscriptions which are not already subscribed.
   *
   * @param email the email
   * @return the list
   */
  @Query("SELECT s FROM Subscription s where s.name not in (select cs.name from CustomerSubscription cs where cs.email = :email)")
  public List<Subscription> findAvailableSubscriptions(String email);

  /**
   * Checks if the subscription exists.
   *
   * @param name the name
   * @return true, if successful
   */
  @Query("SELECT count(s) > 0 FROM Subscription s where s.name = :name")
  public boolean existsAvailableSubscription(String name);
}
