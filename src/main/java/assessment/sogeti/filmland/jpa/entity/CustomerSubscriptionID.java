package assessment.sogeti.filmland.jpa.entity;

import java.io.Serializable;
import javax.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

/**
 * The Class CustomerSubscriptionID.
 */
@Embeddable
@Getter
@Setter
public class CustomerSubscriptionID implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The email. */
  private String email;

  /** The name. */
  private String name;

}
