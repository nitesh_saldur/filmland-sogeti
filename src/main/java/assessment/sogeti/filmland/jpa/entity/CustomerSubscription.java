package assessment.sogeti.filmland.jpa.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

/**
 * The Class CustomerSubscription.
 */
@Entity
@Table(name = "filmlandcustsubscription")
@IdClass(CustomerSubscriptionID.class)

@Getter
@Setter
public class CustomerSubscription {

  /** The email. */
  @Id
  @JsonIgnore
  private String email;

  /** The name. */
  @Id
  private String name;


  /** The remaining content. */
  @Column(name = "remainingcontent")
  private int remainingContent;

  /** The price. */
  private double price;

  /** The start date. */
  @Column(name = "startdate")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
  private Date startDate;

  /** The next payment date. */
  @Column(name = "nextpaymentdate")
  @JsonIgnore
  private Date nextPaymentDate;

}
