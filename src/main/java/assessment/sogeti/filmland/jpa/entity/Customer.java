package assessment.sogeti.filmland.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * The Class Customer.
 */
@Entity
@Table(name = "filmlandcustomer")
@Getter
@Setter
public class Customer {

  /** The email. */
  @Id
  @Column(name = "email")
  private String email;

  /** The password. */
  @Column(name = "password")
  private String password;

}
