package assessment.sogeti.filmland.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * The Class Subscription.
 */
@Entity
@Table(name = "filmlandsubscription")
@Getter
@Setter
public class Subscription {

  /** The name. */
  @Id
  private String name;

  /** The available content. */
  @Column(name = "availablecontent")
  private int availableContent;

  /** The price. */
  private double price;

}
