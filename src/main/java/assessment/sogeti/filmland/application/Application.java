package assessment.sogeti.filmland.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class FilmlandApplication.
 */
@SpringBootApplication(scanBasePackages = {"assessment.sogeti.filmland"})
public class Application {

  /**
   * The main method.
   *
   * @param args the arguments
   */
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
