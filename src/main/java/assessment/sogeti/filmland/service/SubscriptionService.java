package assessment.sogeti.filmland.service;

import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import assessment.sogeti.filmland.jpa.entity.CustomerSubscription;
import assessment.sogeti.filmland.jpa.entity.Subscription;
import assessment.sogeti.filmland.jpa.repository.CustomerSubscriptionRepository;
import assessment.sogeti.filmland.jpa.repository.SubscriptionRepository;
import assessment.sogeti.filmland.rest.model.subscription.CreateSubscriptionRequest;
import assessment.sogeti.filmland.rest.model.subscription.GetSubscriptionResponse;
import assessment.sogeti.filmland.rest.model.subscription.TransferSubscriptionRequest;
import assessment.sogeti.filmland.validator.SubscriptionValidator;

/**
 * The Class SubscriptionService.
 */
@Service
public class SubscriptionService {

  /** The customer subscription repository. */
  @Autowired
  private CustomerSubscriptionRepository customerSubscriptionRepository;

  /** The subscription repository. */
  @Autowired
  private SubscriptionRepository subscriptionRepository;

  /** The subscription validator. */
  @Autowired
  private SubscriptionValidator subscriptionValidator;

  /**
   * Retreive all the subscribed categories and all the available categories other than subscribed
   * categories.
   *
   * @param userName the user name
   * @return the gets the subscription response
   */
  public GetSubscriptionResponse retreiveSubscriptions(String userName) {
    GetSubscriptionResponse response = new GetSubscriptionResponse();
    // If no categories available, set it as empty list
    response.setAvailableCategories(
        Optional.ofNullable(subscriptionRepository.findAvailableSubscriptions(userName))
            .orElseGet(Collections::emptyList));
    // If no categories are subscribed, set it as empty list
    response.setSubscribedCategories(
        Optional.ofNullable(customerSubscriptionRepository.findByEmail(userName))
            .orElseGet(Collections::emptyList));
    return response;
  }

  /**
   * Creates the new subscription for the customer with start date as current date and payment date
   * as date after 2 months.
   *
   * @param createSubscriptionRequest the create subscription request
   */
  public void createSubscription(CreateSubscriptionRequest createSubscriptionRequest) {
    subscriptionValidator.validateCreateSubscription(createSubscriptionRequest);
    GetSubscriptionResponse response = retreiveSubscriptions(createSubscriptionRequest.getEmail());
    // Find the available subscription
    Subscription subscription = response.getAvailableCategories().stream()
        .filter(sub -> sub.getName().equals(createSubscriptionRequest.getAvailableCategory()))
        .findFirst().get();
    CustomerSubscription customerSubscription = new CustomerSubscription();
    customerSubscription.setEmail(createSubscriptionRequest.getEmail());
    customerSubscription.setName(createSubscriptionRequest.getAvailableCategory());
    customerSubscription.setPrice(subscription.getPrice());
    customerSubscription.setRemainingContent(subscription.getAvailableContent());
    Calendar calendar = Calendar.getInstance(Locale.getDefault());
    customerSubscription.setStartDate(calendar.getTime());
    // Add 2 months to current date and set it as payment date
    calendar.add(Calendar.MONTH, 2);
    customerSubscription.setNextPaymentDate(calendar.getTime());
    customerSubscriptionRepository.save(customerSubscription);
  }

  /**
   * Transfer subscription from one customer to another customer. Price is shared between 2 customer
   * equally. Content quantity is shared equally. Start date for other customer's subscription is
   * current date. Payment date of other customer's subscription is 1 month later than current date.
   *
   * @param transferSubscriptionRequest the transfer subscription request
   */
  @Transactional
  public void transferSubscription(TransferSubscriptionRequest transferSubscriptionRequest) {
    subscriptionValidator.validateTransferSubscription(transferSubscriptionRequest);
    CustomerSubscription customerSubscription =
        customerSubscriptionRepository.findByEmailAndName(transferSubscriptionRequest.getEmail(),
            transferSubscriptionRequest.getSubscribedCategory());
    double sharedPrice = customerSubscription.getPrice();
    customerSubscription.setPrice(sharedPrice / 2);
    sharedPrice -= customerSubscription.getPrice();
    // In case of uneven remaining content, higher content goes to first customer
    // and rest goes to second customer
    int remainingContent = customerSubscription.getRemainingContent();
    System.out.println(Double.valueOf(Math.ceil((double) remainingContent / 2)).intValue());
    customerSubscription
        .setRemainingContent(Double.valueOf(Math.ceil((double) remainingContent / 2)).intValue());
    remainingContent -= customerSubscription.getRemainingContent();

    CustomerSubscription newCustomerSubscription = new CustomerSubscription();
    newCustomerSubscription.setEmail(transferSubscriptionRequest.getCustomer());
    newCustomerSubscription.setName(transferSubscriptionRequest.getSubscribedCategory());
    newCustomerSubscription.setPrice(sharedPrice);
    newCustomerSubscription.setRemainingContent(remainingContent);
    Calendar calendar = Calendar.getInstance(Locale.getDefault());
    customerSubscription.setStartDate(calendar.getTime());
    newCustomerSubscription.setStartDate(calendar.getTime());
    calendar.add(Calendar.MONTH, 1);
    newCustomerSubscription.setNextPaymentDate(calendar.getTime());
    customerSubscriptionRepository.save(customerSubscription);
    customerSubscriptionRepository.save(newCustomerSubscription);
  }
}
