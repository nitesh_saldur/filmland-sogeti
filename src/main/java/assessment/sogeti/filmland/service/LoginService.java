package assessment.sogeti.filmland.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import assessment.sogeti.filmland.security.config.AuthTokenUtil;
import assessment.sogeti.filmland.security.config.AuthUserDetailsService;

/**
 * The Class LoginService.
 */
@Service
public class LoginService {

  /** The authentication manager. */
  @Autowired
  private AuthenticationManager authenticationManager;

  /** The auth token util. */
  @Autowired
  private AuthTokenUtil authTokenUtil;

  /** The filmland user details service. */
  @Autowired
  private AuthUserDetailsService filmlandUserDetailsService;

  /**
   * Authenticate the request using authentication manager. Authentication Exception will be thrown
   * in case of invalid password and it will be handled by AuthEntryPint.
   *
   * @param userName the user name
   * @param password the password
   * @return the string
   */
  public String authenticate(String userName, String password) {
    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, password));
    UserDetails userDetails = filmlandUserDetailsService.loadUserByUsername(userName);
    String token = authTokenUtil.generateToken(userDetails);
    return token;
  }

}
