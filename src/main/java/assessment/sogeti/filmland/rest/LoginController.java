package assessment.sogeti.filmland.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import assessment.sogeti.filmland.rest.model.login.LoginRequest;
import assessment.sogeti.filmland.rest.model.login.LoginResponse;
import assessment.sogeti.filmland.service.LoginService;

/**
 * The Class LoginController.
 */
@RestController
@CrossOrigin
@RequestMapping("login")
public class LoginController {

  /** The filmland login service. */
  @Autowired
  private LoginService filmlandLoginService;

  /**
   * Authenticate the email and password.
   *
   * @param loginRequest containg email and password
   * @return the response entity
   */
  @PostMapping
  public ResponseEntity<LoginResponse> authenticate(@RequestBody LoginRequest loginRequest) {
    return ResponseEntity.ok(new LoginResponse(
        filmlandLoginService.authenticate(loginRequest.getEmail(), loginRequest.getPassword())));
  }


}
