package assessment.sogeti.filmland.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import assessment.sogeti.filmland.rest.model.subscription.CreateSubscriptionRequest;
import assessment.sogeti.filmland.rest.model.subscription.GetSubscriptionResponse;
import assessment.sogeti.filmland.rest.model.subscription.TransferSubscriptionRequest;
import assessment.sogeti.filmland.service.SubscriptionService;

/**
 * The Class SubscriptionController.
 */
@RestController
@CrossOrigin
@RequestMapping("subscriptions")
public class SubscriptionController {

  /** The subscription service. */
  @Autowired
  private SubscriptionService subscriptionService;

  /**
   * Retreive subscriptions.
   *
   * @param email the email
   * @return the response entity
   * @throws Exception the exception
   */
  @GetMapping
  public ResponseEntity<GetSubscriptionResponse> retreiveSubscriptions(@RequestParam String email) {
    return ResponseEntity.ok(subscriptionService.retreiveSubscriptions(email));
  }

  /**
   * Creates the new subscription.
   *
   * @param filmlandCreateSubscriptionRequest the filmland create subscription request
   * @return the response entity
   * @throws Exception the exception
   */
  @PostMapping("create")
  public ResponseEntity<GetSubscriptionResponse> createNewSubscription(
      @RequestBody CreateSubscriptionRequest filmlandCreateSubscriptionRequest) {
    subscriptionService.createSubscription(filmlandCreateSubscriptionRequest);
    return ResponseEntity.ok(
        subscriptionService.retreiveSubscriptions(filmlandCreateSubscriptionRequest.getEmail()));
  }

  /**
   * Transfers the subscription from one customer to another.
   *
   * @param filmlandTransferSubscriptionRequest the filmland transfer subscription request
   * @return the response entity
   * @throws Exception the exception
   */
  @PostMapping("transfer")
  public ResponseEntity<GetSubscriptionResponse> transferSubscription(
      @RequestBody TransferSubscriptionRequest filmlandTransferSubscriptionRequest) {
    subscriptionService.transferSubscription(filmlandTransferSubscriptionRequest);
    return ResponseEntity.ok(
        subscriptionService.retreiveSubscriptions(filmlandTransferSubscriptionRequest.getEmail()));
  }

}
