package assessment.sogeti.filmland.rest.model.subscription;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateSubscriptionRequest {

  /** The email. */
  private String email;

  /** The available category. */
  private String availableCategory;

}
