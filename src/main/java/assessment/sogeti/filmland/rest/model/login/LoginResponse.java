package assessment.sogeti.filmland.rest.model.login;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class LoginResponse {

  /** The token. */
  private String token;

}
