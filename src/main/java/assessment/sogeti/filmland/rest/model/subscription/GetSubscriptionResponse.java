package assessment.sogeti.filmland.rest.model.subscription;

import java.util.List;
import assessment.sogeti.filmland.jpa.entity.CustomerSubscription;
import assessment.sogeti.filmland.jpa.entity.Subscription;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetSubscriptionResponse {

  /** The available categories. */
  private List<Subscription> availableCategories;

  /** The subscribed categories. */
  private List<CustomerSubscription> subscribedCategories;

}
