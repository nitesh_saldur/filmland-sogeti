package assessment.sogeti.filmland.rest.model.subscription;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransferSubscriptionRequest {

  /** The email. */
  private String email;

  /** The customer. */
  private String customer;

  /** The subscribed category. */
  private String subscribedCategory;
}
