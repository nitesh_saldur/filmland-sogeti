package assessment.sogeti.filmland.rest.model.login;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequest {

  /** The email. */
  private String email;

  /** The password. */
  private String password;

}
