DROP TABLE IF EXISTS filmlandcustomer;
 
CREATE TABLE filmlandcustomer (
  email VARCHAR2(100) PRIMARY KEY,
  password VARCHAR2(100) NOT NULL,
);
 
INSERT INTO filmlandcustomer (email, password) VALUES ('test123', '$2a$10$G1WvssMD3uxSrakg608Kg.WSHY75ONc1CiugNgzi5y1wTq73wy7kO');
INSERT INTO filmlandcustomer (email, password) VALUES ('test456', '$2a$10$G1WvssMD3uxSrakg608Kg.WSHY75ONc1CiugNgzi5y1wTq73wy7kO');
  
  
 
DROP TABLE IF EXISTS filmlandsubscription;
 
CREATE TABLE filmlandsubscription (
  name VARCHAR2(100) PRIMARY KEY,
  availablecontent INT NOT NULL,
  price DOUBLE NOT NULL
);
 
INSERT INTO filmlandsubscription (name, availablecontent, price) VALUES ('Dutch Films', 10, 4.0);
INSERT INTO filmlandsubscription (name, availablecontent, price) VALUES ('Dutch Series', 20, 6.0);
INSERT INTO filmlandsubscription (name, availablecontent, price) VALUES ('International Films', 5, 8.0);


DROP TABLE IF EXISTS filmlandcustsubscription;
 
CREATE TABLE filmlandcustsubscription (
	email VARCHAR2(100) ,
	name VARCHAR2(100),
  	remainingcontent  INT NOT NULL,
  	startdate DATETIME DEFAULT CURRENT_TIMESTAMP,
  	price DOUBLE NOT NULL,
  	nextpaymentdate DATETIME,
  	 FOREIGN KEY(email) REFERENCES filmlandcustomer,
  	 FOREIGN KEY(name) REFERENCES filmlandsubscription
);


INSERT INTO filmlandcustsubscription (email, name, remainingcontent, startdate, price, nextpaymentdate) VALUES ('test123', 'Dutch Films', 10, CURRENT_TIMESTAMP, 4.0, CURRENT_TIMESTAMP);