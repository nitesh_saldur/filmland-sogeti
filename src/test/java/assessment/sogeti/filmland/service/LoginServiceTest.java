package assessment.sogeti.filmland.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import assessment.sogeti.filmland.security.config.AuthTokenUtil;
import assessment.sogeti.filmland.security.config.AuthUserDetailsService;

@ExtendWith(SpringExtension.class)
class LoginServiceTest {

  @Spy
  @InjectMocks
  private LoginService loginService;
  
  @Mock
  private AuthenticationManager authenticationManager;

  @Mock
  private AuthTokenUtil authTokenUtil;

  @Mock
  private AuthUserDetailsService filmlandUserDetailsService;
  
  @Mock
  private UserDetails userDetails;
  
  @Test
  @DisplayName("test a success scenario")
  void testAuthenticate() {
    String username = "test-user";
    String password = "test-pswd";
    String token = "test-token";
    when(filmlandUserDetailsService.loadUserByUsername(username)).thenReturn(userDetails);
    when(authTokenUtil.generateToken(userDetails)).thenReturn(token);
    assertEquals(loginService.authenticate(username, password), token);
    verify(authenticationManager).authenticate(new UsernamePasswordAuthenticationToken(username, password));
  }

}
