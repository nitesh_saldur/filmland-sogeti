package assessment.sogeti.filmland.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import assessment.sogeti.filmland.jpa.entity.CustomerSubscription;
import assessment.sogeti.filmland.jpa.entity.Subscription;
import assessment.sogeti.filmland.jpa.repository.CustomerSubscriptionRepository;
import assessment.sogeti.filmland.jpa.repository.SubscriptionRepository;
import assessment.sogeti.filmland.rest.model.subscription.CreateSubscriptionRequest;
import assessment.sogeti.filmland.rest.model.subscription.GetSubscriptionResponse;
import assessment.sogeti.filmland.rest.model.subscription.TransferSubscriptionRequest;
import assessment.sogeti.filmland.validator.SubscriptionValidator;

@ExtendWith(SpringExtension.class)
class SubscriptionServiceTest {

  @Spy
  @InjectMocks
  private SubscriptionService subscriptionService;

  @Mock
  private CustomerSubscriptionRepository customerSubscriptionRepository;

  @Mock
  private SubscriptionRepository subscriptionRepository;

  @Mock
  private SubscriptionValidator subscriptionValidator;

  @Test
  @DisplayName("Test a scenario where available categories and subscribed categories are retreived")
  void testRetreiveSubscriptions1() {
    String username = "test-user";
    when(subscriptionRepository.findAvailableSubscriptions(username))
        .thenReturn(createAvilableSubscriptions());
    when(customerSubscriptionRepository.findByEmail(username))
        .thenReturn(createSubscibedCategories());
    GetSubscriptionResponse getSubscriptionResponse =
        subscriptionService.retreiveSubscriptions(username);
    assertEquals(getSubscriptionResponse.getAvailableCategories().size(), 2);
    assertEquals(getSubscriptionResponse.getSubscribedCategories().size(), 1);
  }

  @Test
  @DisplayName("Test a scenario where available categories or subscribed categories are not present then empty list is set in their place")
  void testRetreiveSubscriptions2() {
    String username = "test-user";
    when(subscriptionRepository.findAvailableSubscriptions(username)).thenReturn(null);
    when(customerSubscriptionRepository.findByEmail(username)).thenReturn(null);
    GetSubscriptionResponse getSubscriptionResponse =
        subscriptionService.retreiveSubscriptions(username);
    assertTrue(getSubscriptionResponse.getAvailableCategories().isEmpty());
    assertTrue(getSubscriptionResponse.getSubscribedCategories().isEmpty());
  }

  private List<Subscription> createAvilableSubscriptions() {
    List<Subscription> subscriptions = new ArrayList<>();
    Subscription subscription = new Subscription();
    subscription.setAvailableContent(20);
    subscription.setName("Dutch Series");
    subscription.setPrice(6.0d);
    subscriptions.add(subscription);

    subscription = new Subscription();
    subscription.setAvailableContent(5);
    subscription.setName("International Films");
    subscription.setPrice(8);
    subscriptions.add(subscription);

    return subscriptions;
  }

  private List<CustomerSubscription> createSubscibedCategories() {
    List<CustomerSubscription> customerSubscriptions = new ArrayList<>();
    CustomerSubscription subscription = new CustomerSubscription();
    subscription.setRemainingContent(15);
    subscription.setName("Dutch Films");
    subscription.setPrice(7.0d);
    customerSubscriptions.add(subscription);
    return customerSubscriptions;
  }

  @Test
  @DisplayName("check the email, price, content, start date and payment date of the new subscription")
  void testCreateSubscription() {
    String username = "test-user";
    when(subscriptionRepository.findAvailableSubscriptions(username))
        .thenReturn(createAvilableSubscriptions());
    when(customerSubscriptionRepository.findByEmail(username))
        .thenReturn(createSubscibedCategories());
    CreateSubscriptionRequest createSubscriptionRequest = createSubscriptionRequest();
    subscriptionService.createSubscription(createSubscriptionRequest);
    ArgumentCaptor<CustomerSubscription> argument = ArgumentCaptor.forClass(CustomerSubscription.class);
    verify(customerSubscriptionRepository, atLeastOnce()).save(argument.capture());
    CustomerSubscription actCustSub = argument.getValue();
    assertEquals(actCustSub.getEmail(), "test-user");
    assertEquals(actCustSub.getName(), "International Films");
    assertEquals(actCustSub.getPrice(), 8);
    assertEquals(actCustSub.getRemainingContent(), 5);
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(actCustSub.getStartDate());
    calendar.add(Calendar.MONTH, 2);
    // check payment date is 2 month after the start date, since its a new subscription
    assertEquals(actCustSub.getNextPaymentDate().getTime(), calendar.getTime().getTime());
  }

  private CreateSubscriptionRequest createSubscriptionRequest() {
    CreateSubscriptionRequest createSubscriptionRequest = new CreateSubscriptionRequest();
    createSubscriptionRequest.setAvailableCategory("International Films");
    createSubscriptionRequest.setEmail("test-user");
    return createSubscriptionRequest;
  }

  @Test
  @DisplayName("check the email, price, content, start date and payment date of the transferred subscription")
  void testTransferSubscription() {
    String username = "test-user";
    String category = "Dutch Films";
    when(customerSubscriptionRepository.findByEmailAndName(username, category)).thenReturn(createSubscibedCategories().get(0));
    subscriptionService.transferSubscription(createTransferRequest());
    
    ArgumentCaptor<CustomerSubscription> argument = ArgumentCaptor.forClass(CustomerSubscription.class);
    verify(customerSubscriptionRepository, atLeastOnce()).save(argument.capture());
    //From customer
    CustomerSubscription actCustSub = argument.getAllValues().get(0);
    assertEquals(actCustSub.getPrice(), 3.5d);
    assertEquals(actCustSub.getName(), "Dutch Films");
    assertEquals(actCustSub.getRemainingContent(), 8);
    //To customer
    actCustSub = argument.getAllValues().get(1);
    assertEquals(actCustSub.getPrice(), 3.5d);
    assertEquals(actCustSub.getName(), "Dutch Films");
    assertEquals(actCustSub.getRemainingContent(), 7);
    assertEquals(actCustSub.getEmail(), "test-user2");
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(actCustSub.getStartDate());
    calendar.add(Calendar.MONTH, 1);
    // check payment date is 1 month after the start date, since its a transferred subscription
    assertEquals(actCustSub.getNextPaymentDate().getTime(), calendar.getTime().getTime());
  }
  
  private TransferSubscriptionRequest createTransferRequest() {
    TransferSubscriptionRequest transferSubscriptionRequest = new TransferSubscriptionRequest();
    transferSubscriptionRequest.setCustomer("test-user2");
    transferSubscriptionRequest.setEmail("test-user");
    transferSubscriptionRequest.setSubscribedCategory("Dutch Films");
    return transferSubscriptionRequest;
  }

}
