package assessment.sogeti.filmland.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import assessment.sogeti.filmland.rest.model.login.LoginRequest;
import assessment.sogeti.filmland.rest.model.login.LoginResponse;
import assessment.sogeti.filmland.service.LoginService;

@ExtendWith(SpringExtension.class)
class LoginControllerTest {

  @Spy
  @InjectMocks
  private LoginController loginController;
  
  @Mock
  private LoginService filmlandLoginService; 
  
  @Test
  @DisplayName("Test that token is present in the response provided by service")
  void testAuthenticate() throws Exception {
    String username = "test-user";
    String password = "test-pswd";
    when(filmlandLoginService.authenticate(username, password)).thenReturn("test-token");
    LoginRequest loginRequest = new LoginRequest();
    loginRequest.setEmail(username);
    loginRequest.setPassword(password);
    ResponseEntity<LoginResponse> response = loginController.authenticate(loginRequest);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertEquals(response.getBody().getToken(), "test-token"); 
  }

}
