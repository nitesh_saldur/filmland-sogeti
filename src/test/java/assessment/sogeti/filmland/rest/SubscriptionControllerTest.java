package assessment.sogeti.filmland.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import assessment.sogeti.filmland.rest.model.subscription.CreateSubscriptionRequest;
import assessment.sogeti.filmland.rest.model.subscription.GetSubscriptionResponse;
import assessment.sogeti.filmland.rest.model.subscription.TransferSubscriptionRequest;
import assessment.sogeti.filmland.service.SubscriptionService;

@ExtendWith(SpringExtension.class)
class SubscriptionControllerTest {

  @Spy
  @InjectMocks
  private SubscriptionController subscriptionController;
  @Mock
  private SubscriptionService subscriptionService;
  @Mock
  private GetSubscriptionResponse getSubscriptionResponse;
  
  @BeforeEach
  void setup() {
    when(subscriptionService.retreiveSubscriptions("test-user")).thenReturn(getSubscriptionResponse);
  }
  
  @Test
  @DisplayName("Check the method returns the subscriptions it receives from the service")
  void testRetreiveSubscriptions() {
    ResponseEntity<GetSubscriptionResponse> response = subscriptionController.retreiveSubscriptions("test-user");
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertEquals(response.getBody(), getSubscriptionResponse);
  }

  @Test
  @DisplayName("Check the method first calls the create method of the service and then returns the subscriptions it receives from the service")
  void testCreateNewSubscription() {
    CreateSubscriptionRequest filmlandCreateSubscriptionRequest = new CreateSubscriptionRequest();
    filmlandCreateSubscriptionRequest.setEmail("test-user");
    ResponseEntity<GetSubscriptionResponse> response = subscriptionController.createNewSubscription(filmlandCreateSubscriptionRequest);
    verify(subscriptionService).createSubscription(filmlandCreateSubscriptionRequest);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertEquals(response.getBody(), getSubscriptionResponse);
  }

  @Test
  @DisplayName("Check the method first calls the transfer method of the service and then returns the subscriptions it receives from the service")
  void testTransferSubscription() {
    TransferSubscriptionRequest filmlandTransferSubscriptionRequest = new TransferSubscriptionRequest();
    filmlandTransferSubscriptionRequest.setEmail("test-user");
    ResponseEntity<GetSubscriptionResponse> response = subscriptionController.transferSubscription(filmlandTransferSubscriptionRequest);
    verify(subscriptionService).transferSubscription(filmlandTransferSubscriptionRequest);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
    assertEquals(response.getBody(), getSubscriptionResponse);
  }

}
