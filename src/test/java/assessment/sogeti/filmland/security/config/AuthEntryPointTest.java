package assessment.sogeti.filmland.security.config;

import static org.mockito.Mockito.verify;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class AuthEntryPointTest {
  
  @Spy
  private AuthEntryPoint authEntryPoint;
  
  @Mock
  private HttpServletResponse httpServletResponse; 

  @Test
  @DisplayName("check that unauthorised http status code(401) is set in the response")
  void testCommence() throws IOException, ServletException {
    authEntryPoint.commence(null, httpServletResponse, null);
    verify(httpServletResponse).sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
  }

}
