package assessment.sogeti.filmland.security.config;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import io.jsonwebtoken.ExpiredJwtException;

@ExtendWith(SpringExtension.class)
class AuthRequestFilterTest {

  @Spy
  @InjectMocks
  private AuthRequestFilter authRequestFilter;

  @Mock
  private AuthUserDetailsService filmlandUserService;

  @Mock
  private AuthTokenUtil filmlandTokenUtil;

  @Mock
  private HttpServletRequest request;

  @Mock
  private HttpServletResponse response;

  @Mock
  private FilterChain filterChain;

  @Mock
  private UserDetails userDetails;

  @BeforeEach
  void setup() {
    when(request.getHeader("Authorization")).thenReturn("Bearer bearer-jwt-token");
    when(filmlandTokenUtil.getUsernameFromToken("bearer-jwt-token")).thenReturn("mock-username");
    when(filmlandUserService.loadUserByUsername("mock-username")).thenReturn(userDetails);
    when(filmlandTokenUtil.validateToken("bearer-jwt-token", userDetails)).thenReturn(true);
    SecurityContextHolder.getContext().setAuthentication(null);
  }

  @Test
  @DisplayName("Check a success scenario")
  void testDoFilterInternal1() throws IOException, ServletException {
    authRequestFilter.doFilterInternal(request, response, filterChain);
    assertNotNull(SecurityContextHolder.getContext().getAuthentication());
    verify(filterChain).doFilter(request, response);
  }

  @Test
  @DisplayName("Check that authentication is not set in security context, if JWT token is not present in header")
  void testDoFilterInternal2() throws IOException, ServletException {
    when(request.getHeader("Authorization")).thenReturn(null);
    authRequestFilter.doFilterInternal(request, response, filterChain);
    assertNull(SecurityContextHolder.getContext().getAuthentication());
    verify(filmlandUserService, times(0)).loadUserByUsername("");
    verify(filterChain).doFilter(request, response);
  }

  @Test
  @DisplayName("Check that authentication is not set in security context, if there is no username in the token")
  void testDoFilterInternal3() throws IOException, ServletException {
    when(filmlandTokenUtil.getUsernameFromToken("bearer-jwt-token")).thenReturn(null);
    authRequestFilter.doFilterInternal(request, response, filterChain);
    assertNull(SecurityContextHolder.getContext().getAuthentication());
    verify(filmlandUserService, times(0)).loadUserByUsername("");
    verify(filterChain).doFilter(request, response);
  }

  @Test
  @DisplayName("Check that authentication is not set in security context, if there is exception while retreiving username from the token")
  void testDoFilterInternal4() throws IOException, ServletException {
    when(filmlandTokenUtil.getUsernameFromToken("bearer-jwt-token")).thenThrow(new ExpiredJwtException(null, null,"mock exception"));
    authRequestFilter.doFilterInternal(request, response, filterChain);
    assertNull(SecurityContextHolder.getContext().getAuthentication());
    verify(filmlandUserService, times(0)).loadUserByUsername("");
    verify(filterChain).doFilter(request, response);
  }
  
  @Test
  @DisplayName("Check that authentication is not set in security context, if jwt token is not valid")
  void testDoFilterInternal5() throws IOException, ServletException {
    when(filmlandTokenUtil.validateToken("bearer-jwt-token", userDetails)).thenReturn(false);
    authRequestFilter.doFilterInternal(request, response, filterChain);
    assertNull(SecurityContextHolder.getContext().getAuthentication());
    verify(filmlandUserService, times(1)).loadUserByUsername("mock-username");
    verify(filterChain).doFilter(request, response);
  }

}
