package assessment.sogeti.filmland.security.config;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import assessment.sogeti.filmland.jpa.entity.Customer;
import assessment.sogeti.filmland.jpa.repository.CustomerRepository;

@ExtendWith(SpringExtension.class)
class AuthUserDetailsServiceTest {

  @Spy
  @InjectMocks
  private AuthUserDetailsService authUserDetailsService;

  @Mock
  private CustomerRepository filmlandCustomerRepository;

  @Test
  @DisplayName("Test the success scenario where userdetails is retreived")
  void testLoadUserByUsername1() {
    Customer filmlandCustomer = new Customer();
    filmlandCustomer.setEmail("test-email");
    filmlandCustomer.setPassword("test-password");
    when(filmlandCustomerRepository.findById("test-name"))
        .thenReturn(Optional.of(filmlandCustomer));
    UserDetails userDetails = authUserDetailsService.loadUserByUsername("test-name");
    assertEquals(userDetails.getUsername(), "test-email");
    assertEquals(userDetails.getPassword(), "test-password");
  }

  @Test
  @DisplayName("Test the scenario when customer is not found, Exception should be thrown")
  void testLoadUserByUsername2() {
    when(filmlandCustomerRepository.findById("test-name")).thenReturn(Optional.ofNullable(null));
    assertThrows(UsernameNotFoundException.class, () -> {
      authUserDetailsService.loadUserByUsername("test-name");
    });
  }

}
