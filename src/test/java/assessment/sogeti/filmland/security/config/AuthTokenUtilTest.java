package assessment.sogeti.filmland.security.config;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@TestPropertySource(properties = {"filmland.jwt.secret=test-secret",})
@ContextConfiguration(classes = AuthTokenUtil.class)
@ExtendWith(SpringExtension.class)
class AuthTokenUtilTest {

  @Autowired
  private AuthTokenUtil authTokenUtil;

  private static final long CURRENT_DATE = new Date().getTime();

  private static final String JWT_TOKEN =
      Jwts.builder().setClaims(new HashMap<>()).setSubject("test-subject").setIssuedAt(new Date(CURRENT_DATE))
          .setExpiration(new Date(CURRENT_DATE + AuthTokenUtil.JWT_TOKEN_VALIDITY))
          .signWith(SignatureAlgorithm.HS512, "test-secret").compact();

  @Test
  void testGetUsernameFromToken() {
    assertEquals(authTokenUtil.getUsernameFromToken(JWT_TOKEN), "test-subject");
  }

  @Test
  void testGenerateToken() {
    User user = new User("test-user", "test-password", new ArrayList<>());
    String token = authTokenUtil.generateToken(user);
    assertEquals(authTokenUtil.getUsernameFromToken(token), "test-user");
  }

  @Test
  void testValidateToken() {
    User user = new User("test-user", "test-password", new ArrayList<>());
    String token = authTokenUtil.generateToken(user);
    assertTrue(authTokenUtil.validateToken(token, user));
    assertFalse(authTokenUtil.validateToken(JWT_TOKEN, user));
  }

}
