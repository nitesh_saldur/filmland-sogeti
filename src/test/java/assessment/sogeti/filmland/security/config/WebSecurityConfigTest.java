package assessment.sogeti.filmland.security.config;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.userdetails.DaoAuthenticationConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class WebSecurityConfigTest {

  @Spy
  @InjectMocks
  private WebSecurityConfig webSecurityConfig;
  
  @Mock
  private AuthEntryPoint filmlandAuthEntryPoint;

  @Mock
  private AuthUserDetailsService filmlandUserService;

  @Mock
  private AuthRequestFilter filmlandAuthRequestFilter;
  
  @SuppressWarnings({"rawtypes", "unchecked"})
  @Test
  @DisplayName("test that user detail service and password encode is set")
  void testConfigureGlobal() throws Exception {
    AuthenticationManagerBuilder authenticationManagerBuilder = mock(AuthenticationManagerBuilder.class);
    DaoAuthenticationConfigurer daoAuthenticationConfigurer =  mock(DaoAuthenticationConfigurer.class);
    BCryptPasswordEncoder bCryptPasswordEncoder = mock(BCryptPasswordEncoder.class);
    when(webSecurityConfig.passwordEncoder()).thenReturn(bCryptPasswordEncoder);
    when(authenticationManagerBuilder.userDetailsService(filmlandUserService)).thenReturn(daoAuthenticationConfigurer);
    webSecurityConfig.configureGlobal(authenticationManagerBuilder);
    verify(authenticationManagerBuilder).userDetailsService(filmlandUserService);
    verify(authenticationManagerBuilder.userDetailsService(filmlandUserService)).passwordEncoder(bCryptPasswordEncoder);
    reset(webSecurityConfig);
  }

  @Test
  void testPasswordEncoder() {
    assertEquals(webSecurityConfig.passwordEncoder().getClass(), BCryptPasswordEncoder.class);
  }

}
