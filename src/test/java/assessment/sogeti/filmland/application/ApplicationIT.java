package assessment.sogeti.filmland.application;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.io.UnsupportedEncodingException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import assessment.sogeti.filmland.rest.model.login.LoginResponse;

/**
 * This class does functional testing for valid and invalid login. It also test the Get Subscription API
 * @author Nitesh Saldur
 *
 */
@SpringBootTest
@AutoConfigureMockMvc
class ApplicationIT {

  @Autowired
  private MockMvc mockMvc;
  
  @Autowired
  private ObjectMapper objectMapper;
  
  @Test
  @DisplayName("Test successful login")
  void testLogin1() throws Exception {
    String loginRequest = "{\r\n" + 
        "    \"email\": \"test123\",\r\n" + 
        "    \"password\": \"test123\"\r\n" + 
        "}";
    mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(loginRequest))
    .andExpect(status().isOk());
    
  }
  
  @Test
  @DisplayName("Test invalid login. Unauthorised status code should be returned")
  void testLogin2() throws Exception {
    String loginRequest = "{\r\n" + 
        "    \"email\": \"test123\",\r\n" + 
        "    \"password\": \"test121\"\r\n" + 
        "}";
    mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(loginRequest))
    .andExpect(status().isUnauthorized());
  }
  
  @Test
  @DisplayName("Test get subscription without valid authentication")
  void testGetSubscriptions1() throws Exception {
    mockMvc.perform(get("/subscriptions").param("email", "test123"))
    .andExpect(status().isUnauthorized());
  }
  
  @Test
  @DisplayName("Test get subscription with valid authentication")
  void testGetSubscriptions2() throws Exception {
    String jwtToken = getLoginToken();
    mockMvc.perform(get("/subscriptions").param("email", "test123").header("Authorization", "Bearer "+jwtToken))
    .andExpect(status().isOk());
  }
  

  private String getLoginToken() throws UnsupportedEncodingException, Exception {
    String loginRequest = "{\r\n" + 
        "    \"email\": \"test123\",\r\n" + 
        "    \"password\": \"test123\"\r\n" + 
        "}";
    String response = mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(loginRequest))
    .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
    LoginResponse loginResponse = objectMapper.readValue(response, LoginResponse.class);
    return loginResponse.getToken();
  }

}
