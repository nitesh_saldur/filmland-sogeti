package assessment.sogeti.filmland.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import assessment.sogeti.filmland.exception.FilmlandException;
import assessment.sogeti.filmland.jpa.repository.CustomerRepository;
import assessment.sogeti.filmland.jpa.repository.CustomerSubscriptionRepository;
import assessment.sogeti.filmland.jpa.repository.SubscriptionRepository;
import assessment.sogeti.filmland.rest.model.subscription.CreateSubscriptionRequest;
import assessment.sogeti.filmland.rest.model.subscription.TransferSubscriptionRequest;

@ExtendWith(SpringExtension.class)
class SubscriptionValidatorTest {

  @Spy
  @InjectMocks
  private SubscriptionValidator subscriptionValidator;
  
  @Mock
  private CustomerSubscriptionRepository customerSubscriptionRepository;
  @Mock
  private SubscriptionRepository subscriptionRepository;
  @Mock
  private CustomerRepository filmlandCustomerRepository;
  
  @BeforeEach
  void setup() {
    when(filmlandCustomerRepository.existsById("test-user")).thenReturn(true);
    when(filmlandCustomerRepository.existsById("test-user2")).thenReturn(true);
    when(customerSubscriptionRepository.existsByEmailAndName("test-user", "test-category")).thenReturn(false);
    when(customerSubscriptionRepository.existsByEmailAndName("test-user", "test-category1")).thenReturn(true);
    when(customerSubscriptionRepository.existsByEmailAndName("test-user2", "test-category1")).thenReturn(false);
    when(subscriptionRepository.existsAvailableSubscription("test-category")).thenReturn(true);
    when(subscriptionRepository.existsAvailableSubscription("test-category1")).thenReturn(true);
  }
  
  @Test
  @DisplayName("Test success scenario where customer with email exist, customer is not subscribed to category and category is valid category ")
  void testValidateCreateSubscription1() {
    subscriptionValidator.validateCreateSubscription(createSubscriptionRequest());
  }
  
  @Test
  @DisplayName("Test scenario when customer with email doesnt exist, FilmlandException should be thrown")
  void testValidateCreateSubscription2() {
    when(filmlandCustomerRepository.existsById("test-user")).thenReturn(false);
    FilmlandException filmlandException = assertThrows(FilmlandException.class, () ->{ subscriptionValidator.validateCreateSubscription(createSubscriptionRequest());});
    assertEquals(filmlandException.getMessage(), "Email test-user does not exist");
  }
  
  @Test
  @DisplayName("Test scenario when customer is already subscribed to category, FilmlandException should be thrown")
  void testValidateCreateSubscription4() {
    when(customerSubscriptionRepository.existsByEmailAndName("test-user", "test-category")).thenReturn(true);
    FilmlandException filmlandException = assertThrows(FilmlandException.class, () ->{ subscriptionValidator.validateCreateSubscription(createSubscriptionRequest());});
    assertEquals(filmlandException.getMessage(), "Subscription test-category is already subscribed");    
  }
  
  @Test
  @DisplayName("Test scenario when category is invalid, FilmlandException should be thrown")
  void testValidateCreateSubscription3() {
    when(subscriptionRepository.existsAvailableSubscription("test-category")).thenReturn(false);
    FilmlandException filmlandException = assertThrows(FilmlandException.class, () ->{ subscriptionValidator.validateCreateSubscription(createSubscriptionRequest());});
    assertEquals(filmlandException.getMessage(), "Subscription test-category is not available");    
  }
  
  private CreateSubscriptionRequest createSubscriptionRequest() {
    CreateSubscriptionRequest createSubscriptionRequest = new CreateSubscriptionRequest();
    createSubscriptionRequest.setAvailableCategory("test-category");
    createSubscriptionRequest.setEmail("test-user");
    return createSubscriptionRequest;
  }

  @Test
  @DisplayName("Test success scenario where one customer should be able to transfer subscribed category to another customer")
  void testValidateTransferSubscription1() {
    subscriptionValidator.validateTransferSubscription(createTransferRequest());
  }
  
  @Test
  @DisplayName("Test scenario when email from second customer is not valid")
  void testValidateTransferSubscription2() {
    when(filmlandCustomerRepository.existsById("test-user2")).thenReturn(false);
    FilmlandException filmlandException = assertThrows(FilmlandException.class, () ->{subscriptionValidator.validateTransferSubscription(createTransferRequest());});
    assertEquals(filmlandException.getMessage(), "Email test-user2 does not exist");
  }
  
  @Test
  @DisplayName("Test scenario when second customer is already subscribed to category")
  void testValidateTransferSubscription3() {
    when(customerSubscriptionRepository.existsByEmailAndName("test-user2", "test-category1")).thenReturn(true);
    FilmlandException filmlandException = assertThrows(FilmlandException.class, () ->{subscriptionValidator.validateTransferSubscription(createTransferRequest());});
    assertEquals(filmlandException.getMessage(), "Subscription test-category1 is already subscribed");
  }
  
  @Test
  @DisplayName("Test scenario when first customer is not subscribed to category")
  void testValidateTransferSubscription4() {
    when(customerSubscriptionRepository.existsByEmailAndName("test-user", "test-category1")).thenReturn(false);
    FilmlandException filmlandException = assertThrows(FilmlandException.class, () ->{subscriptionValidator.validateTransferSubscription(createTransferRequest());});
    assertEquals(filmlandException.getMessage(), "Subscription test-category1 is not subscribed");
  }

  private TransferSubscriptionRequest createTransferRequest() {
    TransferSubscriptionRequest transferSubscriptionRequest = new TransferSubscriptionRequest();
    transferSubscriptionRequest.setCustomer("test-user2");
    transferSubscriptionRequest.setEmail("test-user");
    transferSubscriptionRequest.setSubscribedCategory("test-category1");
    return transferSubscriptionRequest;
  }
}
