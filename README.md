# Sogeti Filmland Assessment

## Tool/Technologies/Framework
- Java 8
- Junit 5
- Mockito
- Spring Boot 2.1.6.RELEASE
- Eclipse IDE
- Maven 3.6.0
- Embedded Tomcat Server
- Embedded H2 Database 

## Implementation
- H2 database is initiated on server startup. Schema and Data is present in 'sql/import.sql'.
- On server start
	* 2 customer are created with email 'test123' and 'test456'. both have the same password 'test123'.
	* 3 categories are created. 'Dutch Films', 'Dutch Series' and 'International Films'.
	* One customer has a existing subscription of category 'Dutch Films'.
- For security implementation, JSon Web Token is used for authentication purpose. On successful login, JWT token will be received in the output that should be used for subsequent API calls as 'Authorization Bearer' token in the request header. Access is denied for other API's if the token is not present in the header.
- Below validation/Logic are implemented
	* Customer with valid credential can only login.
	* Customer cannot subscribe to already existing category.
	* Customer can only subscribe to available category.
	* On new subscription, payment date will be set as 2 month from current date since the first month will be free.
	* Customer can only transfer the category, if the category is already subscribed.
	* Customer can only transfer the category, if the other customer email is valid.
	* Customer can only transfer the category, if the other customer doesnt already subscribed to the category.
	* Price of the category will be shared equally between customer.
	* Payment date for the other customer will be set as 1 month from current date.
	
## Instruction
- mvn clean install && java -jar target/filmland-assessment-1.0.0-SNAPSHOT.jar
- Run the above command from base directory. command with compile, unit test and start the tomcat server 

## API
| API | HTTP Method | URL | Request Body | Header |
| --- | ----------- | --- | ------------ | ------ |
| Login | HTTP POST | http://localhost:8080/login | { "email": "test123", "password": "test123" } | |
| Get Subscription | HTTP GET | http://localhost:8080/subscriptions?email=test123 |  | Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0MTIzIiwiZXhwIjoxNTcyMDI0MTU1LCJpYXQiOjE1NzIwMjI5NTV9.lDYCh46q8xz9VFSc3C81J5D2fE3Tq-Va8CegUIIEzdIzIxjMbNv4lav72IhpSWR70lAgR3je08HEvt8qdGzJIA |
| Create New Subscription | HTTP POST | http://localhost:8080/subscriptions/create | { "email": "test123", "availableCategory": "International Films" } | Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0MTIzIiwiZXhwIjoxNTcyMDI0MTU1LCJpYXQiOjE1NzIwMjI5NTV9.lDYCh46q8xz9VFSc3C81J5D2fE3Tq-Va8CegUIIEzdIzIxjMbNv4lav72IhpSWR70lAgR3je08HEvt8qdGzJIA |
| Transfer Subscription | HTTP POST | hhttp://localhost:8080/subscriptions/transfer | { "email": "test123", "customer": "test456", "subscribedCategory": "Dutch Films" } | Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0MTIzIiwiZXhwIjoxNTcyMDI0MTU1LCJpYXQiOjE1NzIwMjI5NTV9.lDYCh46q8xz9VFSc3C81J5D2fE3Tq-Va8CegUIIEzdIzIxjMbNv4lav72IhpSWR70lAgR3je08HEvt8qdGzJIA |